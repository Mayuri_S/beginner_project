import React, { Component, useDebugValue } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import { List, ListItem } from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'

class Confirm extends Component {
    continue = e => {
        e.preventDefault()
        this.props.nextStep()
    }
    back = e => {
        e.preventDefault()
        this.props.prevStep()
    }
    render() {
        const { value } = this.props
        return (<MuiThemeProvider>
            <React.Fragment>
                <AppBar title='Enter Details'></AppBar>
                <List>
                    <ListItem primaryText="FirstName"
                        secondaryText={value.firstN}> </ListItem>
                    <ListItem primaryText="lastName"
                        secondaryText={value.lastN}> </ListItem>
                    <ListItem primaryText="City"
                        secondaryText={value.city}> </ListItem>
                </List>
                <RaisedButton label='continue' primary={true} style={styles.button} onClick={this.continue}></RaisedButton>
                <RaisedButton label='back' primary={false} style={styles.button} onClick={this.back}></RaisedButton>

            </React.Fragment>
        </MuiThemeProvider>)
    }
}
const styles = {
    button: {
        margin: 15

    }
}

export default Confirm