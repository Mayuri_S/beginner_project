import React, { Component } from 'react'
import UserFormDetails from './UserFormDetails'
import App from '../App'
import FormPersonalDetails from './FormPersonalDetails'
import Confirm from './Confirm'
import Success from './Success'


class UserForm extends Component {

    state = {
        step: 1,
        firstNerr: '',
        firstN: '',
        lastN: '',
        email: '',
        occupation: '',
        city: '',
        bio: '',

    }



    nextStep = () => {
        this.setState(
            prevs => {
                return { step: prevs.step + 1 }
            }
        )
    }
    prevStep = () => {
        this.setState(
            prevs => {
                return { step: prevs.step - 1 }
            }
        )
    }
    handle = input => e => {
        this.setState({
            [input]: e.target.value,

        }, () => { this.afterSetStateCalled(input) })


    }
    afterSetStateCalled = (input) => {
        if (input === 'firstN' && (this.state.firstNerr !== '')) {
            console.log('inside afterSetStateCalled' + this.state.firstNerr)
            this.setState({
                firstNerr: ''
            }
            )
        }
    }
    errors = (field, mes) => {
        console.log('inside erros')
        if (field === 'firstN') {
            this.setState({
                firstNerr: mes
            }, console.log('inside erros' + this.state.firstNerr))


        }
    }
    render() {
        const { step } = this.state;
        const { firstN, lastN, email, occupation, city, bio, firstNerr: firstNerr } = this.state;
        const value = { firstN, lastN, email, occupation, city, bio, firstNerr }
        switch (step) {
            case 1: return (
                <UserFormDetails nextStep={this.nextStep} handleChange={(input) => this.handle(input)} value={value} errors={(field, mes) => this.errors(field, mes)}>


                </UserFormDetails>


            )
            case 2: return (<FormPersonalDetails nextStep={this.nextStep} handleChange={(input) => this.handle(input)} value={value} prevStep={this.prevStep}>


            </FormPersonalDetails>)


            case 3: return (<Confirm nextStep={this.nextStep} value={value} prevStep={this.prevStep}>


            </Confirm>)

            case 4: return (<Success></Success>)
        }
    }
}
export default UserForm