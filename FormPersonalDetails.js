import React, { Component, useDebugValue } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

class FormPersonalDetails extends Component {
    continue = e => {
        e.preventDefault()
        this.props.nextStep()
    }

    back = e => {
        e.preventDefault()
        this.props.prevStep()
    }
    render() {
        const { value, handleChange } = this.props
        return (<MuiThemeProvider>
            <React.Fragment>
                <AppBar title='Enter Details'></AppBar>
                <TextField hintText="enter Occupation" floatingLabelText="Occupation" defaultValue={value.occupation} onChange={handleChange('occupation')}></TextField>
                <br></br>
                <TextField hintText="enter City" floatingLabelText="City" defaultValue={value.city} onChange={handleChange('city')}></TextField>
                <br></br>
                <TextField hintText="enter Bio" floatingLabelText="Bio" defaultValue={value.bio} onChange={handleChange('bio')}></TextField>
                <br></br>
                <RaisedButton label='continue' primary={true} style={styles.button} onClick={this.continue}></RaisedButton>
                <RaisedButton label='back' primary={false} style={styles.button} onClick={this.back}></RaisedButton>

            </React.Fragment>
        </MuiThemeProvider>)
    }
}
const styles = {
    button: {
        margin: 15

    }
}

export default FormPersonalDetails