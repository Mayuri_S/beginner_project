import React, { Component, useDebugValue } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import '../App.css';

class UserFormDetails extends Component {

    continue = e => {
        e.preventDefault()
        if (this.props.firstN != '') {
            console.log(' inside not null continue' + this.props.firstN)
            this.props.nextStep()
        }
        else {
            console.log(this.props.firstN + 'inside null first name continue')
            return this.props.errors('firstN', 'Please enter')
        }



    }
    render() {

        const { value, handleChange } = this.props

        return (<MuiThemeProvider>
            <React.Fragment>

                <AppBar title='Enter Details'></AppBar>
                <TextField hintText="enter fn" floatingLabelText="First Name" defaultValue={value.firstN} onChange={handleChange('firstN')}></TextField>
                <small className={"small" + (value.firstNerr ? "show " : "")}  >{value.firstNerr} </small>
                <br></br>
                <TextField hintText="enter ln" floatingLabelText="Larst Name" defaultValue={value.lastN} onChange={handleChange('lastN')}></TextField>
                <br></br>
                <TextField hintText="enter email" floatingLabelText="Email" defaultValue={value.email} onChange={handleChange('email')}></TextField>
                <br></br>
                <RaisedButton label='continue' primary={true} style={styles.button} onClick={this.continue}></RaisedButton>

            </React.Fragment>
        </MuiThemeProvider>)
    }
}
const styles = {
    button: {
        margin: 15

    }
}

export default UserFormDetails